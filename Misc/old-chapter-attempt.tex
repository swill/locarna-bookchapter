\documentclass[a4paper,10pt]{article}

\usepackage[a4paper,margin=3cm]{geometry}
\usepackage{todonotes}
\usepackage{hyperref}

\usepackage[framemethod=TikZ]{mdframed}

\newenvironment{shell}{\begin{mdframed}[]\relax\tt}{\end{mdframed}}
\newenvironment{shellout}{\par\begin{minipage}{\textwidth}\relax\tt}{\end{minipage}}
  
\begin{document}

\begin{center}
  \textbf{\LARGE Comparative RNA Secondary Structure Prediction}

  \bigskip
  \textit{\Large Sebastian Will $^1$}
\end{center}

\medskip
\noindent
{\small $^1$ Institute for Theoretical Chemistry, University of Vienna,
Währinger Strasse 17, A-1090 Vienna, Austria}

\medskip
\noindent
Email: will@tbi.univie.ac.at

\begin{abstract}
   Prediction of RNA secondary structure is a highly useful computational
   tool in the study of RNA structural biology. It is even more powerful in
   the comparative analysis of related RNAs. Predicting the secondary
   structure of RNAs from several homologous sequences can drastically
   improves the reliability over predictions from single sequences.
   Extremely successful and well-established in the study of RNAs is the
   prediction of structure from known, often hand-curated alignments. Going
   beyond such approaches, the chapter demonstrates how secondary structure can be
   predicted from \emph{unaligned} RNA sequences, how alignments can be
   generated from such RNAs and how related, 'alignable' RNAs are
   identified in a large set of
   potentially unrelated RNAs. The flexibility to analyze RNAs of a priori unknown
   structure is often
   crucial for automatized methods. As well, such techniques can be highly beneficial for
   the detailed analysis of single RNA families.  The gold-standard for
   this task is simultaneous alignment and folding (SA\&F) as introduced 
   by David Sankoff in 1985.  Whereas such methods originally required
   extreme computational resources, they can today be routinely applied to
   various RNA analysis tasks thanks to recent advances as well as robust
   and flexible implementations. This chapter provides a walk through the
   LocARNA software package, which implements a comprehensive set of fast
   and highly customizable Sankoff-like simultaneous folding and alignment
   variants. We are going to discuss major features and important built-in
   optimizations; the text describes use-cases for clustering, global and
   local alignment, computation of local alignment quality, RNA boundary
   prediction, alignment and folding of multiple RNAs, use of prior
   knowledge in the form of anchors and structure constraints.
 
   \textbf{Keywords:} energy directed comparative folding, RNA secondary
 structure, RNA alignment, alignment reliability \end{abstract}

\section{Introduction}

\todo{Figures?}

\begin{itemize}
  \item computational RNA secondary structure prediction in general
  \item comparative prediction to improve prediction reliability
  \item comparative prediction to identify homology; reveal RNA funtion
  \item simultaneous alignment and folding as gold standard
    (non-simultaneous methods are flawed)
  \item historic development: efficient but extreme cost; heavy-weight and
    light-weight lines; simplification of PMcomp, sparsification in LocARNA
  \item How to read the chapter / chapter overview
\end{itemize}

\section{Materials}
\subsection{Hardware and System Requirements}


Linux systems on PC hardware provide an ideal environment for developing
and running bioinformatics software. As most free bioinformatics software,
also the discussed tools were designed primarily having the use via Linux
(or UNIX) command line in mind. Consequently, for re-cooking the examples
and applying the discussed software in general, Linux PCs should be the
first choice. Nevertheless, the software can as well be installed and
applied on MacOS X or Windows systems. While installation under MacOS X is 'natively'
supported via pre-compiled packages, Windows users should consider using the Linux
subsystem, which allows to install and use the software in the same way as under Linux.

The software doesn't require special hardware or puts high demands in terms
of memory or speed for small to medium applications. Typical PCs or
notebooks are therefore sufficient for running the examples and trying out all 
the functionality of the tools. For performing computationally more intense analysis,
the software supports the use of multi-threading on multi-core machines and
distributed computation on compute clusters.

\subsection{Software and Data Resources}

\paragraph{LocARNA package}

This package provides several tools for the comparison of unaligned RNAs
based on their sequence and structure similarities. The chapter refers to
LocARNA 2.0.0.

%Online ressources
The LocARNA package is free software under GPL 3.0 license and therefore
available in full source code (even if users typically install
from binary packages provided via conda/bioconda). It is hosted at
\url{https://github.com/s-will/LocARNA}, where latest source code releases
are provided. LocARNA's original home page is found at
\url{http://www.bioinf.uni-freiburg.de/Software/LocARNA} and a web server
at
\url{http://rna.informatik.uni-freiburg.de/LocARNA}.

%Dependencies
The LocARNA package requires the Vienna RNA package. Parts of the package, in particular multiple
alignment, require Perl 5.

\paragraph{Vienna RNA package}

The Vienna RNA package consists of various tools for predicting RNA
secondary structures. \mbox{LocARNA} utilizes its
implementation of high-quality RNA energy models and fast, advanced RNA
folding algorithms through Vienna RNA's C-library and its command line tools.
LocARNA requires at least Vienna RNA 2.4.13.

%Online ressources
This software resides
at \url{https://www.tbi.univie.ac.at/RNA}. Binary packages are
available for various systems from the same site and via conda/bioconda.
Its source code is available
at \url{https://github.com/ViennaRNA/ViennaRNA}. 

\paragraph{Rfam database}

The Rfam database (\url{http://rfam.xfam.org}) collects information on the known RNA families. For each family, it
provides multiple alignments and consensus structures. It is thus a
central reference ressource for the comparative analysis of RNAs. Technically, the
Rfam is build on the concept of consensus models (CMs) and tools in the
software package Infernal. The database also provides CMs to describe each family.


\paragraph{Conda}

The conda package manager can be used for easily installing binary packages
of the tools. The conda distribution Miniconda provides a very good basis
to install LocARNA and the Vienna RNA pacakge; it is available at
\url{https://conda.io/en/latest/miniconda.html}.

\section{Methods}
\subsection{General Remarks}

This chapter describes how to run the software tools from the command line,
even if much functionality of the LocARNA package is as well accessible via
web servers. While web servers support experimenting with the tools and running
smaller applications, only the command line unlocks their full potential.

In general, for bioinformatics analysis, using the command line has
significant advantages over web servers or graphical user interfaces, as it
allows experienced users to harvest the power of the command line (and
thus scripting) for
automation. The increased possibilities make it more than worth-while to
overcome the initially steep learning curve.

\subsection{Installation of the LocARNA Package, General Usage, Getting
Help}

\subsubsection{Recommended Installation (from Binary Pacakges)}

LocARNA can be most easily installed from a binary package on Linux, MacOS
X, or when using the Linux subsystem for Windows. This mechanism
uses the package management system Conda.

\paragraph{Install the package manager Conda}
Unless you already installed conda, it is recommended to install Miniconda
following the instructions of the Bioconda project:
\url{https://bioconda.github.io/user/install.html#install-conda} 

These instructions boil down to running a few commands from the
command line. Thus, the very first step is to start a terminal program. If you
install under Linux (or Linux subsystem), then enter the commands

\begin{shell}
  wget
  https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86\_64.sh\\
  sh Miniconda3-latest-Linux-x86\_64.sh
\end{shell}

or if you have MacOS X, enter

\begin{shell}
  wget
  https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86\_64.sh\\
  sh Miniconda3-latest-MacOSX-x86\_64.sh
\end{shell}

These commands download the installer script and run it. Please follow its
instructions to install miniconda in your home directory.

Finally, it is recommended to set up conda channels (i.e.,
repositories for conda packages), by
\begin{shell}
  conda config --add channels defaults\\
  conda config --add channels bioconda\\
  conda config --add channels conda-forge
\end{shell}

If you want to go on using Conda directly after installing it, it is
typically necessary to re-start your Terminal.

\paragraph{Installing LocARNA via Conda}
If Conda is already installed and channels are set up properly, e.g.\ after
following the instructions above, one can install LocARNA with all its
requirements by a single command.

\begin{shell}
  conda install locarna
\end{shell}

Before using programs installed via Conda, make sure Conda
is active (or actually, Conda's \emph{base environment}, but we can ignore
these details here). It is activated by 
\begin{shell}
   conda activate
\end{shell}
As one would expect, it can be dectivated by the corresponding command
\texttt{conda deactivate}.

\paragraph{Test your LocARNA Installation}

One can now briefly test the succesful installation of LocARNA, e.g. by
\begin{shell}
  mlocarna --version
\end{shell}
This should report the LocARNA version by printing a string like
\begin{shellout}
  LocARNA 2.0.0
\end{shellout}
The version number should be at least 2.0.0.


\subsubsection{Alternative Installations (from Source)}

\paragraph{From Github release}
only describe installation from release, describe installation to
non-standard directories

\paragraph{Experts only: from Github branch}

\subsubsection{General usage}

\subsubsection{How to get help}
\subsubsection{Input file formats}

\subsection{Pairwise Simultaneous Alignment and Folding}
\subsubsection{Global, Local, Glocal}
\subsubsection{Heuristics}
\subsection{Constraints}
\subsubsection{Structure constraints}
\subsubsection{Anchor constraints}
\subsection{Multiple Alignment and Clustering}
\subsubsection{Multiple Alignment}
\subsubsection{Realignment}
in distance to other alignment, realignment with column constraints
\subsubsection{Clustering}
compute cluster trees, limit multiple alignment size, find relevant subtrees
\subsection{Reliability: alignment quality, boundary prediction}

\subsection{Using the LocARNA webserver}

\section{Appendix}
\subsection{File formats}
\subsubsection{General formats with proprietary extensions/specifics}
\subsubsection{Proprietary---PP 2.0}
\subsection{MLocARNA output directory}

\end{document}

