# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import subprocess
import sys
import os
import glob
import re
import RNA

import seaborn as sns
import numpy as np
import itertools
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle



locarna_home = "/home/will/Research/Projects/LocARNA/_inst/bin"
mlocarna_cmd = f"{locarna_home}/mlocarna"

RNAalifold_options = ["--ribosum_scoring", "--cfactor=0.6", "--nfactor=0.5"]

# -

_ = subprocess.run([mlocarna_cmd,"--version"])

# +
input_rnas = "tRNA_5.fa"
tgt_dir = input_rnas.replace(".fa",".out")

#input_rnas = "/home/will/Research/Projects/Bralibase/Data/k5/tRNA/tRNA.apsi-40.sci-108.no-1.raw.fa"
#input_rnas = "/home/will/Research/Projects/Bralibase/Data/k5/tRNA/tRNA.apsi-40.sci-118.no-1.raw.fa"
#tgt_dir = "tRNA_5.out"

subprocess.run([mlocarna_cmd,input_rnas,"--consensus-structure=alifold",
                f"--tgtdir={tgt_dir}", "--width=60"
               ])

import skbio
from skbio.tree import TreeNode
with open(f"{tgt_dir}/results/result.tree") as fh:
    tree = skbio.read(fh,format="newick", into=TreeNode)
print(tree.ascii_art())


# -

# !RNAfold --noPS < /home/will/Research/Projects/Bralibase/Data/k5/tRNA/tRNA.apsi-40.sci-118.no-1.raw.fa

# +
def nogrid(dotfile):
    with open(dotfile) as fh:
        content = fh.readlines()
    with open(dotfile,'w') as fh:
        for line in content:
            if line == 'drawgrid\n': continue
            fh.write(line)
    
def ps2pdf(infile, outfile, removein=True, crop=True):
    subprocess.run(["epstopdf", infile, outfile])
    if removein:
        os.remove(infile)
    if crop:
        subprocess.run(["pdfcrop", outfile, f"{outfile}.cropped"])
        os.remove(outfile)
        os.rename(f"{outfile}.cropped", outfile)


# -

for filename in ['aln', 'alirna']:
    ps2pdf(f'{tgt_dir}/results/{filename}.ps',f'{tgt_dir}/results/{filename}.pdf')


# +
def traverse(tree, level=0):
    name = tree.name.replace(' ','_')
    if tree.has_children():
        name = f"intermediate{name}"
    print("  "*level, name)
    for subtree in tree:
        traverse(subtree,level+1)
        
traverse(tree)
# -

## convert intermediates
intermediates = glob.glob(f"{tgt_dir}/intermediates/*.aln")
for alnfile in intermediates:
    alnfile = alnfile.replace(".aln","")
    ppfile = f"{alnfile}.pp"

    subprocess.run(["RNAalifold"]+RNAalifold_options+["-p", "--color", f"{alnfile}.aln", "--aln"])
    ps2pdf("alirna.ps", f"{alnfile}_alirna.pdf")
    nogrid("alidot.ps")
    ps2pdf("alidot.ps", f"{alnfile}_alidot.pdf")
    ps2pdf("aln.ps", f"{alnfile}_aln.pdf")

    subprocess.run([f"{locarna_home}/pp2dot", f"{ppfile}", f"{alnfile}_ppdot.ps"])
    nogrid(f"{alnfile}_ppdot.ps")
    ps2pdf(f"{alnfile}_ppdot.ps", f"{alnfile}_ppdot.pdf")     

# convert input files
inputs = glob.glob(f"{tgt_dir}/input/*")
for infile in inputs:
    try:
        with open(infile,"r") as fh:
            headerline = fh.readline()
        assert(headerline=="#PP 2.0\n")
    except:
        continue

    print("CONVERT",infile)

    subprocess.run([f"{locarna_home}/pp2dot", infile, f"{infile}_ppdot.ps"])
    nogrid(f"{infile}_ppdot.ps")
    ps2pdf(f"{infile}_ppdot.ps", f"{infile}_ppdot.pdf")   

# +
subprocess.run(["RNAfold",f"-i{tgt_dir}/input/input.fa"])
output = subprocess.run([f"{locarna_home}/mlocarna_nnames",f"{tgt_dir}/input/input.fa"],stdout=subprocess.PIPE)

output = output.stdout.decode().split('\n')

nnames = dict()
for i in output:
    i = i.split()
    if i:
        k, v = i[0],i[2]
        nnames[k] = v

for k in nnames.keys():
    v = nnames[k]
    print("PLOT",k,v)
    ps2pdf(f"{k}_ss.ps",f"{tgt_dir}/input/{v}_ss.pdf")


# -

# # Probabilistic banding

## read fasta
def readfasta(file):
    with open(file) as fh:
        d = dict()
        for line in fh:
            if line[0]=='>':
                name=line[1:].strip()
            else:
                d[name]=line.strip()
    return d
seqs = readfasta(f'{tgt_dir}/input/input.fa')

# +
inputs = nnames.keys()
outdir = 'Traces'

threshold = 1e-5
gamma = 1/3

def trace_plot(x,y,lenX,lenY,threshold,gamma):
    subprocess.run(["/home/will/Research/Projects/LocARNA/_inst/bin/locarna",
        x,y,"--write-trace-probs=trace"])

    trace = np.zeros((lenX+1,lenY+1), dtype=float)
    with open('trace') as fh:
        for line in fh:
            x,y,p = line.split()
            x,y,p = int(x),int(y),float(p)
            trace[x,y] = p

    cmap = plt.get_cmap("Blues")
    cmap.set_gamma(gamma)
            
    ax = sns.heatmap(trace, square=True,
                     xticklabels=10, yticklabels=10,
                     cmap=cmap)
    
    if threshold is not None:
        # selected cells
        for i in range(lenX+1):
            min_j,max_j = 0,0
            for j in range(lenY+1):
                if trace[i,j]>=threshold:
                    min_j = j
                    break
            for j in reversed(range(lenY+1)):
                if trace[i,j]>=threshold:
                    max_j = j
                    break
            rect = Rectangle((min_j,i),max_j-min_j+1,1,linewidth=0.75,edgecolor="red",facecolor=None,alpha=0.2)
            ax.add_patch(rect)


try:
    os.mkdir(outdir)
except:
    pass

for x,y in list(itertools.product(inputs,inputs)): #[:5]
    if x>=y: continue

    lenX = len(seqs[x])
    lenY = len(seqs[y])
    
    filename = f'{outdir}/{x}-{y}_trace'
    print(x,y)

    x=nnames[x]
    y=nnames[y]
    
    x = f'tRNA_5.out/input/{x}'
    y = f'tRNA_5.out/input/{y}'
    
    trace_plot(x,y,lenX,lenY,threshold,gamma) 
        
    plt.savefig(f'{filename}.pdf',format='pdf',
                bbox_inches='tight', pad_inches=0)
    plt.show()


# -

# ## Ad-hoc banding

# +
def banding_plot(lenX,lenY,Delta,trace=None):
    filename = f'banding-{lenX}-{lenY}-{Delta}'

    if trace is not None:
        filename += "-trace"
    
    a = np.zeros((lenX+1,lenY+1))

    cmap = plt.get_cmap("Blues")
    ax = sns.heatmap(a, square=True,
                     xticklabels=10, yticklabels=10,
                     cmap=cmap, cbar=None)

    minmax_j = [[lenY+1,0] for _ in range(lenX+1)]
    
    if trace is None:
        for i in range(lenX+1):
            minmax_j[i] = (np.floor(i*lenY/lenX - Delta), np.ceil(i*lenY/lenX + Delta))
    else:
        for i in range(lenX+1):
            for i1 in range(max(0,i-Delta),min(lenX,i+Delta)+1):
                minmax_j[i1][0] = min(trace[i][0],minmax_j[i1][0])
                minmax_j[i1][1] = max(trace[i][1],minmax_j[i1][1])
            
            
    # selected cells
    for i in range(lenX+1):
        
        min_j, max_j = minmax_j[i]
        min_j = max(min_j,0)
        max_j = min(max_j,lenY)
        
        rect = Rectangle((min_j,i),max_j-min_j+1,1,linewidth=0.75,edgecolor="red",facecolor='white',alpha=0.5)
        ax.add_patch(rect)

        if trace is not None:
            rect = Rectangle((trace[i][0],i),trace[i][1]-trace[i][0]+1,1,linewidth=0.75,facecolor="blue",alpha=0.6)
            ax.add_patch(rect)
        
    plt.savefig(f'{filename}.pdf',format='pdf',
                bbox_inches='tight', pad_inches=0)
    plt.show()
    
# banding_plot(70,  70, 20)
banding_plot(70, 140, 10)
inslen = 70
trace = ([(i,i) for i in range(30)]
        + [(30,30+inslen)]
        + [(i+inslen,i+inslen) for i in range(31,71)]
        )
banding_plot(70, 140, 10, trace)
# -

# # LocARNA basepair/ensemble sparsification

# +
import matplotlib.lines

def to_base_zero(m):
    dimx=len(m)-1
    dimy=len(m[0])-1
    m0 = np.zeros((dimx,dimy),dtype=float)
    for i in range(dimx):
        for j in range(dimy):
            m0[i,j] = m[i+1,j+1]
    return m0

def dotplot(seq,threshold,gamma,bpp=None):
    if bpp is None:
        fc = RNA.fold_compound(seq)
        fc.mfe()
        fc.pf()
        bpp = np.array(fc.bpp())
        bpp = to_base_zero(bpp)
    
    lenX = len(seq)

    cmap = plt.get_cmap("Blues")
    cmap.set_gamma(gamma)
            
    def label(i):
        if i>0 and i%10==0:
            return i
    ticks = []
    ax = sns.heatmap(bpp, square=True,
                     xticklabels=ticks, yticklabels=ticks,
                     cmap=cmap)
    for i in range((lenX+9)//10):
        ax.axhline(i*10,0,lenX,color="black",linestyle="dashed",linewidth=0.5)
        ax.axvline(i*10,0,lenX,color="black",linestyle="dashed",linewidth=0.5)
    
    ax.add_artist(matplotlib.lines.Line2D([0,lenX],[0,lenX],color='green'))
    
    if threshold is not None:
        # selected cells
        for i in range(lenX):
            for j in range(lenX):
                if bpp[i,j] >= threshold:
                    rect = Rectangle((j,i),1,1,linewidth=0.75,edgecolor="red",facecolor=None,alpha=0.5)
                    ax.add_patch(rect)



threshold = 0.01
gamma = 1/3

for x in inputs: #[:5]
    
    filename = f'{outdir}/{x}_dp'
    print(x)
    dotplot(seqs[x],threshold,gamma) 
        
    plt.savefig(f'{filename}.pdf',format='pdf',
                bbox_inches='tight', pad_inches=0)
    plt.show()

# +
# # copy dotplots to input

import shutil
for x in nnames: #[:5]
    shutil.copy(f'Dotplots/{x}_dp.pdf',f'{tgt_dir}/input/{nnames[x]}_dp.pdf')
# -

# !cat tRNA_5.out/intermediates/intermediate1.pp

# +
# create intermediate dotplots
## convert intermediates

import re
import collections

def read_pp_bpp(filename):
    
    alignment = collections.defaultdict(str)
    bpplist = list()
    
    with open(filename) as fh:
        section = None
        for line in fh:
            line = line.strip()
            if line == "": continue

            if section is None:
                if re.match("#PP 2.0",line):
                    section = "TOP"
            elif section == "TOP":
                m = re.match("#SECTION (.*)",line)
                if m:
                    section = m[1]
                elif line[0]=="#":
                    pass
                else:
                    print(line)
                    name,seq = re.split(r'\s+',line)
                    alignment[name] += seq
                    
            elif section == "BASEPAIRS":
                if line and line[0]!='#':
                    i,j,p = line.split(' ')
                    i,j,p = int(i), int(j), float(p)                    
                    bpplist.append((i-1,j-1,p))
    
    length = len(list(alignment.values())[0])
    bpp = np.zeros((length,length),dtype=float)
    for i,j,p in bpplist:
        bpp[i,j] = p
    
    return alignment,bpp
    
intermediates = glob.glob(f"{tgt_dir}/intermediates/*.pp")
for filename in intermediates:
    alignment, bpp = read_pp_bpp(filename)
    dotplot(seqs[x],threshold=0.001,gamma=gamma,bpp=bpp) 
    
    pdffilename = re.sub('.pp$','_dp.pdf',filename)
    plt.savefig(pdffilename, format='pdf',
                bbox_inches='tight', pad_inches=0)
    plt.show()
    
# -


